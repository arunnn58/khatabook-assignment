package khatabook;


import java.time.LocalDate;
import java.time.Month;
import java.util.Scanner;

public class Khatabook {
    
 


    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        
        ShopData sd=new ShopData();
        
        
        sd.customer[0] = new Customer(1001, "Naveen", "9876543210", 27, "ganeshnagar", "chennai", 606040,1000);
        sd.customer[1] = new Customer(1002, "Natraj", "7845612938", 34, "kk.nagar", "chennai", 600028,4000);
        sd.customer[2] = new Customer(1003, "Ram", "8976542234", 45, "ashoknagar", "chennai", 600032,2000);
        sd.customer[3] = new Customer(1004, "Saravanan", "7898675642", 43, "kumarannagar", "chennai", 622001,3000);
        
        sd.ords[0]=new Orders(201,1001,LocalDate.of(2022, 9, 10));
        sd.ords[0].itemlist=new Orderlist[2];
            sd.ords[0].itemlist[0]=new Orderlist(502,25,20,500);
            sd.ords[0].itemlist[1]=new Orderlist(501,50,10,500);
            
        sd.ords[1]=new Orders(202,1002,LocalDate.of(2022, 9, 20));    
        sd.ords[1].itemlist=new Orderlist[2];
            sd.ords[1].itemlist[0]=new Orderlist(505,50,40,2000);
            sd.ords[1].itemlist[1]=new Orderlist(506,100,20,2000);
            
        sd.ords[2]=new Orders(203,1003,LocalDate.of(2022, 9, 30));    
        sd.ords[2].itemlist=new Orderlist[2];
            sd.ords[2].itemlist[0]=new Orderlist(504,150,10,1500);
            sd.ords[2].itemlist[1]=new Orderlist(506,25,20,500);    
            
        sd.ords[3]=new Orders(204,1004,LocalDate.of(2022, 10, 5));    
        sd.ords[3].itemlist=new Orderlist[2];
            sd.ords[3].itemlist[0]=new Orderlist(505,50,40,2000);
            sd.ords[3].itemlist[1]=new Orderlist(506,50,20,1000);        
            
            //int prodId,int qua,double eachprice,double price
        
        
        sd.prods[0] = new Product(501, "5Star", 10, "cadbury", 30);
        sd.prods[0].stockload=new Stock[4];
        sd.prods[1] = new Product(502, "Diarymilk", 20, "cadbury", 40);
        sd.prods[1].stockload=new Stock[4];
        sd.prods[2] = new Product(503, "Candykitchen", 30, "homemade", 20);
        sd.prods[2].stockload=new Stock[4];
        sd.prods[3] = new Product(504, "Snikers", 10, "fruit & nut", 20);
        sd.prods[3].stockload=new Stock[4];
        sd.prods[4] = new Product(505, "Hershey's", 40, "whole almonds", 30);
        sd.prods[4].stockload=new Stock[4];
        sd.prods[5] = new Product(506, "Nestley", 20, "kitkat", 0);
        sd.prods[5].stockload=new Stock[4];
        
        
        
        sd.prods[0].stockload[0]=new Stock(40,LocalDate.of(2022,9,1),9);
        sd.prods[0].stockload[1]=new Stock(30,LocalDate.of(2022,9,15),9);
        sd.prods[0].stockload[2]=new Stock(30,LocalDate.of(2022,10,1),9);
        sd.prods[0].stockload[3]=new Stock(56,LocalDate.of(2022,10,15),9);
        
        sd.prods[1].stockload[0]=new Stock(16,LocalDate.of(2022,8,15),19);
        sd.prods[1].stockload[1]=new Stock(25,LocalDate.of(2022,9,1),19);
        sd.prods[1].stockload[2]=new Stock(16,LocalDate.of(2022,9,15),19);
        sd.prods[1].stockload[3]=new Stock(16,LocalDate.of(2022,10,1),19);
        
        sd.prods[2].stockload[0]=new Stock(15,LocalDate.of(2022,8,15),29);
        sd.prods[2].stockload[1]=new Stock(25,LocalDate.of(2022,9,1),29);
        sd.prods[2].stockload[2]=new Stock(56,LocalDate.of(2022,9,15),29);
        sd.prods[2].stockload[3]=new Stock(76,LocalDate.of(2022,10,1),29);
        
        sd.prods[3].stockload[0]=new Stock(20,LocalDate.of(2022, 8, 2),9);
        sd.prods[3].stockload[1]=new Stock(55,LocalDate.of(2022,9,1),9);
        sd.prods[3].stockload[2]=new Stock(36,LocalDate.of(2022,9,15),9);
        sd.prods[3].stockload[3]=new Stock(76,LocalDate.of(2022,10,1),9);
        
        sd.prods[4].stockload[0]=new Stock(20,LocalDate.of(2022, 8, 2),39);
        sd.prods[4].stockload[1]=new Stock(55,LocalDate.of(2022,9,1),39);
        sd.prods[4].stockload[2]=new Stock(36,LocalDate.of(2022,9,15),39);
        sd.prods[4].stockload[3]=new Stock(76,LocalDate.of(2022,10,1),39);
        
        sd.prods[5].stockload[0]=new Stock(20,LocalDate.of(2022, 8, 2),19);
        sd.prods[5].stockload[1]=new Stock(55,LocalDate.of(2022,9,1),19);
        sd.prods[5].stockload[2]=new Stock(36,LocalDate.of(2022,9,15),19);
        sd.prods[5].stockload[3]=new Stock(76,LocalDate.of(2022,10,1),19);
        
        System.out.println("Successfully loaded....");
        
        
        
        
        
        
        
        
        
        
        boolean exits=true;
        
        while(exits)
        {
     
            System.out.print("\nWelcome to K-mart");
            
            System.out.println("\n+"+"-".repeat(19)+"+");
            System.out.format("|%-19s|","1.Customer");
            
            System.out.println("\n+"+"-".repeat(19)+"+");
            System.out.format("|%-19s|","2.Staticstics");
            
            System.out.println("\n+"+"-".repeat(19)+"+");
            System.out.format("|%-19s|","3.Inventory");
            
            System.out.println("\n+"+"-".repeat(19)+"+");
            System.out.format("|%-19s|","4.Exit");
            System.out.println("\n+"+"-".repeat(19)+"+");
            
            
            
            CustomerMenu cus=new CustomerMenu();
            ProductMenu prod=new ProductMenu();
            Statistics stat=new Statistics();
            int opt=0;
       
            opt=sc.nextInt();
            
            switch(opt)
            {
                case 1:
                    cus.customerMenu(sd);
                    break;
                case 2:
                    stat.statis(sd);
                    break;
                case 3:
                    prod.menu(sd);
                    break;
                case 4:
                    System.out.print("\nThank you....\uD83D\uDE00");
                    //System.out.println("Thank you...+1F600");
                     exits=false;
                    break;
//                case 5:
//                        for (int i=0;i<sd.prods.length;i++)
//                        {
//                            System.out.print("\n"+sd.prods[i]);}
//                    break;
                default:
                    System.out.print("\nPlease enter valid input");
            
            }
            
        }
        
        
    }
    
}
