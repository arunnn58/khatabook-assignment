package khatabook;

import java.util.Scanner;

public class CusStatis {

    Scanner sc = new Scanner(System.in);

    public void cussta(ShopData sd) {

        boolean exit = true;

        while (exit) {
            System.out.print("\n+" + "-".repeat(25) + "+");
            System.out.print("\n|1.High balance customer  |");
            System.out.print("\n+" + "-".repeat(25) + "+");
            System.out.print("\n|2.Customer order details |");
            System.out.print("\n+" + "-".repeat(25) + "+");
            System.out.print("\n|3.Exit                   |");
            System.out.print("\n+" + "-".repeat(25) + "+");

            int opt = sc.nextInt();
            switch (opt) {
                case 1:
                    System.out.print("Enter minimum limit :");
                    int mini = sc.nextInt();

                    System.out.print("\n+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(9) + "+");

                    System.out.format("\n|%-12s|%-15s|%-9s|",
                            "CustomerId", "CustomerName", "Balance");

                    System.out.print("\n+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(9) + "+");

                    for (int i = 0; i < sd.customer.length; i++) {
                        if (sd.customer[i].getBalance() >= mini) {

                            System.out.format("\n|%-12s|%-15s|%9.2f|",
                                    sd.customer[i].getCustomerId(),
                                    sd.customer[i].getName(),
                                    sd.customer[i].getBalance());
                        }
                    }
                    System.out.print("\n+" + "-".repeat(12) + "+" + "-".repeat(15) + "+" + "-".repeat(9) + "+");

                    break;

                case 2:
                    System.out.print("\nEnter CustomerId :");
                    int orcus = sc.nextInt();

                    for (int i = 0; i < sd.ords.length; i++) {

                        if (orcus == sd.ords[i].getCUSID()) {
                            System.out.print("\n+" + "-".repeat(38) + "+");
                            System.out.format("\n|%-20s%19s" , getname(sd, orcus) , "|");
                            System.out.print("\n|" + sd.ords[i].orddate + "                            |");
                            System.out.print("\n+" + "-".repeat(38) + "+");
                            System.out.format("\n|%-7s|%-9s|%-10s|%-9s|",
                                    "ProdId", "Quantity", "EachPrice", "Price");

                            System.out.print("\n+" + "-".repeat(7) + "+" + "-".repeat(9) + "+"
                                    + "-".repeat(10) + "+" + "-".repeat(9) + "+");

                            for (int j = 0; j < sd.ords[i].itemlist.length; j++) {
                                System.out.print("\n" + sd.ords[i].itemlist[j]);
                            }

                            System.out.print("\n+" + "-".repeat(7) + "+" + "-".repeat(9) + "+"
                                    + "-".repeat(10) + "+" + "-".repeat(9) + "+");
                        }
                    }

                    break;
                case 3:
                    exit = false;
                    break;
                default:
                    System.out.print("\nPlease enter valid input");
            }
        }

    }

    public String getname(ShopData sd, int id) {
        String a = "";
        for (int k = 0; k < sd.customer.length; k++) {
            if (id == sd.customer[k].getCustomerId()) {
                a = sd.customer[k].getName();
            }
        }

        return a;
    }
}
