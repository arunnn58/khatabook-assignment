
package khatabook;

public class Orderlist {

     int prodId;
     int quantity;
     double eachprice;
     double price;
     
     Orderlist()
     {}
     
     Orderlist(int prodId,int qua,double eachprice,double price){
     this.prodId=prodId;
     this.quantity=qua;
     this.eachprice=eachprice;
     this.price=price;
     }
     
     public String toString(){
         
     return String.format("|%-7d|%-9d|%10.2f|%9.2f|",prodId,quantity,eachprice,(quantity*eachprice));
     
     }
    
}
