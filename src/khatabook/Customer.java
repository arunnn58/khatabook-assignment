package khatabook;

import java.util.Scanner;

public class Customer {

    boolean status = true;
    private String name;
    private String[] phonenumber;
    private Address address;
    private int  customerId;
    private double balance;
    private double paid;

   
    
    Customer(int customerId, String name, String phonenumber, int doorno, String streetname, String city, int pincode) {

        this.customerId = customerId;
        this.name = name;
        this.phonenumber = new String[1];
        this.phonenumber[this.phonenumber.length - 1] = phonenumber;
        this.address = new Address(doorno, streetname, city, pincode);
     
    }

    Customer(int customerId, String name, String phonenumber, int doorno, String streetname, String city, int pincode,double balance) {

        this.customerId = customerId;
        this.name = name;
        this.phonenumber = new String[1];
        this.phonenumber[this.phonenumber.length - 1] = phonenumber;
        this.address = new Address(doorno, streetname, city, pincode);
        this.balance=balance;
    }

    public String[] addphonenum() {
        String[] tem = phonenumber;
        phonenumber = new String[phonenumber.length + 1];
        for (int i = 0; i < tem.length; i++) {
            phonenumber[i] = tem[i];
        }
        return phonenumber;
    }
    
     public void setPaid(double paid) {
        this.paid = paid;
    }

    public double getPaid() {
        return paid;
    }

    public String getName() {
        return name;
    }

    public String getPhonenumber() {
        return phonenumber[0];
    }

    public Address getAddress() {
        return address;
    }

    public double getBalance() {
        return balance;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhonenumber(String[] phonenumber) {
        this.phonenumber = phonenumber;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String toString() {
        return String.format("\n|%-5d|%-15s|%-12s|%-7d|%-15s|%-8s|%-8d|%9.2f|",
                                customerId,name, phonenumber[0],
                                address.doorno, address.streetname,address.city,address.pincode,balance);
     // System.out.format("\n|%-5d|%-15s|%-12s|%-7d|%-15s|%-8s|%-8d|%9.2f|", 
//                                    sd.customer[i].getCustomerId(), 
//                                    sd.customer[i].getName(), 
//                                    sd.customer[i].getPhonenumber(), 
//                                    sd.customer[i].getAddress().doorno,
//                                    sd.customer[i].getAddress().streetname, 
//                                    sd.customer[i].getAddress().city, 
//                                    sd.customer[i].getAddress().pincode,
//                                    sd.customer[i].getBalance());
    }

}
