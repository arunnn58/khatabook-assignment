
package khatabook;

import java.util.Scanner;
import java.time.LocalDate;
import java.util.Arrays;

public class Product {

      private final int PRODUCTID;
      private final String BRAND;
      private double price;
      private String productName;
      
      int available;
      
      Stock[] stockload=new Stock[0];
      


      
     
      
      Product(){
          PRODUCTID=0;
          BRAND="";
      }
      
    Product(int productId,String productName,double price,String BRAND,int stock)
      {
         this.PRODUCTID=productId;
         this.productName=productName;
         this.price=price;
         this.BRAND=BRAND;
         this.available+=stock;
         
      }
    
   public Stock[] incsto(){
      Stock[] tem=stockload;
      stockload=new Stock[stockload.length+1];
      //stock=Arrays.copyOf(tem,0);
      for (int i=0;i<tem.length;i++)
      {
      stockload[i]=tem[i];}
      return stockload;
   }
    
   @Override
public String toString(){
    
     return String.format("\n|%-8d|%-20s|%-20s|%8s|%-14s|", PRODUCTID,BRAND,productName,price,available);
}



    
      
    public int getProductId() {
        return PRODUCTID;
    }

    

    public double getPrice() {
        return price;
    }

    public String getProductName() {
        return productName;
    }
    
    public void setPrice(int price) {
        this.price = price;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
      
      
      
      
      
      
    
}
