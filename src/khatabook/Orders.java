package khatabook;

import java.time.LocalDate;
import java.util.Scanner;

public class Orders {

    private final int ORDERID;
    private final int CUSID;

    Orderlist[] itemlist = new Orderlist[0];

    double totalprice;
    boolean status;

    LocalDate orddate;

    Scanner sc = new Scanner(System.in);

    public Orders() {
        this.ORDERID = 0;
        this.CUSID = 0;
    }

    public Orders(int ORDERID, int CUSID, LocalDate dat) {
        this.ORDERID = ORDERID;
        this.CUSID = CUSID;
        this.orddate = dat;
    }

    public int getORDERID() {
        return ORDERID;
    }

    public int getCUSID() {
        return CUSID;
    }

    public Orderlist[] inceitls() {
        Orderlist[] tem = itemlist;
        itemlist = new Orderlist[itemlist.length + 1];
        for (int i = 0; i < tem.length; i++) {
            itemlist[i] = tem[i];
        }
        return itemlist;
    }

    public Orders[] inceor(Orders[] ords) {
        Orders[] tem = ords;
        ords = new Orders[ords.length + 1];
        for (int i = 0; i < tem.length; i++) {
            ords[i] = tem[i];
        }
        return ords;
    }

    public void placeorder(ShopData sd) {
        int cusId = 0;
        boolean cusidnotavi = true;
        while (cusidnotavi) {
            System.out.print("\nEnter customerId :");
            cusId = sc.nextInt();
            for (int i = 0; i < sd.customer.length; i++) {
                if (sd.customer[i].getCustomerId() == cusId) {
                    cusidnotavi = false;
                    break;
                }
                if (sd.customer[i].getCustomerId() != cusId && (i == sd.customer.length - 1)) {
                    System.out.println("\nCustomerId not available...!");
                }
            }
        }

        int id = 0;
        if (sd.ords.length == 0) {
            id = 201;
        } else {
            id = sd.ords[sd.ords.length - 1].ORDERID + 1;
        }
        //creatind new order...
        sd.ords = inceor(sd.ords);
        sd.ords[sd.ords.length - 1] = new Orders(id, cusId, LocalDate.now());
        char over = ' ';
        int proid = 0, quan = 0;
        boolean notavailable = false;
        boolean prodnotavail = true;

        do {
            do {
                System.out.print("\nEnter the productId :");
                proid = sc.nextInt();

                System.out.print("\nEnter quantity :");
                quan = sc.nextInt();

                for (int i = 0; i < sd.prods.length; i++) {
                    if ((sd.prods[i].getProductId() == proid)) {
                        prodnotavail = false;
                        notavailable = false;
                        if (sd.prods[i].available < quan) {
                            notavailable = true;
                        }
                    }
                }

                if (prodnotavail) {
                    System.out.print("\nPlease check your productId");
                }
                if (notavailable) {
                    System.out.print("\nSorry...! Your product available is not enough...!");
                }
            } while (notavailable || prodnotavail);

            sd.ords[sd.ords.length - 1].itemlist = sd.ords[sd.ords.length - 1].inceitls();

            sd.ords[sd.ords.length - 1].itemlist[sd.ords[sd.ords.length - 1].itemlist.length - 1] = new Orderlist();
            sd.ords[sd.ords.length - 1].itemlist[sd.ords[sd.ords.length - 1].itemlist.length - 1].prodId = proid;
            sd.ords[sd.ords.length - 1].itemlist[sd.ords[sd.ords.length - 1].itemlist.length - 1].quantity = quan;

            for (int j = 0; j < sd.prods.length; j++) {
                if (proid == sd.prods[j].getProductId()) {
                    sd.ords[sd.ords.length - 1].itemlist[sd.ords[sd.ords.length - 1].itemlist.length - 1].eachprice = sd.prods[j].getPrice();
                    sd.prods[j].available -= quan;
                    sd.ords[sd.ords.length - 1].itemlist[sd.ords[sd.ords.length - 1].itemlist.length - 1].price = quan * sd.prods[j].getPrice();
                }
            }

            System.out.print("\nEnter 'n' if order completed or press any...");
            over = Character.toLowerCase(sc.next().charAt(0));

        } while (over != 'n');

        for (int i = 0; i < sd.ords[sd.ords.length - 1].itemlist.length; i++) {
            sd.ords[sd.ords.length - 1].totalprice += sd.ords[sd.ords.length - 1].itemlist[i].price;

        }

        //bill printing...
        System.out.print("\n+" + "-".repeat(76) + "+");

        for (int i = 0; i < sd.customer.length; i++) {
            if (cusId == sd.customer[i].getCustomerId()) {
                System.out.format("\n|%2s %-16s %-8s %7s|%s%38s|", " ", "Customer Name :", sd.customer[i].getName(), " ", "/", " ");
                System.out.format("\n|%2s %-16s %-8.2f %7s|%s%33s|", " ", "Old Balance :", sd.customer[i].getBalance(), " ", "\\ mart", " ");

            }
        }
        System.out.print("\n+" + "-".repeat(76) + "+");
        System.out.format("\n|%-4s|%-11s|%-20s|%-15s|%-9s|%-12s|",
                "S.No", "ProductId", "Product Name", "Price of each", "Quantity", "Price");
        System.out.print("\n+" + "-".repeat(76) + "+");

        for (int i = 0; i < sd.ords[sd.ords.length - 1].itemlist.length; i++) {

            System.out.format("\n|%-4d|%-11d|%-20s|%-15.2f|%-9d|%12.2f|",
                    (i + 1),
                    sd.ords[sd.ords.length - 1].itemlist[i].prodId,
                    getName(sd.ords[sd.ords.length - 1].itemlist[i].prodId, sd),
                    getPrice(sd.ords[sd.ords.length - 1].itemlist[i].prodId, sd),
                    sd.ords[sd.ords.length - 1].itemlist[i].quantity,
                    sd.ords[sd.ords.length - 1].itemlist[i].price);

        }
        System.out.print("\n+" + "-".repeat(76) + "+");

        System.out.format("\n|%s%53s|%12.2f|", sd.ords[sd.ords.length - 1].orddate, "Total", sd.ords[sd.ords.length - 1].totalprice);

        System.out.print("\n+" + "-".repeat(76) + "+");

        for (int i = 0; i < sd.customer.length; i++) {
            if (sd.ords[sd.ords.length - 1].CUSID == sd.customer[i].getCustomerId()) {
                sd.customer[i].setBalance(sd.ords[sd.ords.length - 1].totalprice + sd.customer[i].getBalance());
            }
        }

    }

    public double getPrice(int p, ShopData sd) {
        double pri = 0;
        for (int i = 0; i < sd.prods.length; i++) {
            if (sd.prods[i].getProductId() == p) {
                pri = sd.prods[i].getPrice();
            }
        }
        return pri;
    }

    public String getName(int a, ShopData sd) {
        String res = "";
        for (int i = 0; i < sd.prods.length; i++) {
            if (sd.prods[i].getProductId() == a) {
                res = sd.prods[i].getProductName();
                break;
            }
        }
        return res;
    }

}
