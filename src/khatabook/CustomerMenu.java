package khatabook;

import java.util.Scanner;

public class CustomerMenu {

    
   //ProductMenu prod = new ProductMenu(); //to calling place order throw product class

    public Customer[] createcus(Customer[] customer) {
        Customer[] tem = customer;
        customer = new Customer[customer.length + 1];
        for (int i = 0; i < tem.length; i++) {
            customer[i] = tem[i];
        }
        return customer;
    }

    public void customerMenu(ShopData sd) {

        
        //customerId, name, phonenumber, doorno, streetname, city, pincode
        Orders ord=new Orders();

        Scanner sc = new Scanner(System.in);
        boolean exit = true;

        while (exit) {

            System.out.print("\nCustomer Menu");
            System.out.print("\n+" + "-".repeat(18) + "+");
            System.out.print("\n|1.Add customer    |");
            System.out.print("\n+" + "-".repeat(18) + "+");
            System.out.print("\n|2.View customer   |");
            System.out.print("\n+" + "-".repeat(18) + "+");
            System.out.print("\n|3.Delete          |");
            System.out.print("\n+" + "-".repeat(18) + "+");
            System.out.print("\n|4.Update customer |");
            System.out.print("\n+" + "-".repeat(18) + "+");
            System.out.print("\n|5.PlaceOrder      |");
            System.out.print("\n+" + "-".repeat(18) + "+");
            System.out.print("\n|6.Payment         |");
            System.out.print("\n+" + "-".repeat(18) + "+");
            System.out.print("\n|7.Exit            |");
            System.out.print("\n+" + "-".repeat(18) + "+");
//            System.out.print("\n+"+"-".repeat(18)+"+");
//            System.out.format("\n|%s|","1.Add ");
            int opt = sc.nextInt();

            switch (opt) {
                case 1:

                    int customerId = 0;

                    if (sd.customer.length == 0) {

                        customerId = 1001;
                    } else {

                        customerId = sd.customer[sd.customer.length - 1].getCustomerId() + 1;
                    }

                    System.out.print("\nEnter the customerName :");
                    String name = sc.next();
                    System.out.print("\nEnter the phonenumber :");
                    String phonenumber = sc.next();
                    System.out.print("\nEnter the doornumber :");
                    int doorno = sc.nextInt();
                    System.out.print("\nEnter the streetname :");
                    String streetname = sc.next();
                    System.out.print("\nEnter the city :");
                    String city = sc.next();
                    System.out.print("\nEnter the pincode :");
                    int pincode = sc.nextInt();

                    sd.customer = createcus(sd.customer);
                    sd.customer[sd.customer.length - 1] = new Customer(customerId, name, phonenumber, doorno, streetname, city, pincode);
                    System.out.print("\nCustomer successfully created...\n");

                    break;

                case 2:
                    boolean exits2=true;
                  while(exits2){
                      System.out.print("\n+" + "-".repeat(20) + "+");
                      System.out.print("\n|1.View all customer |");
                      System.out.print("\n+" + "-".repeat(20) + "+");
                      System.out.print("\n|2.View a particular |");
                      System.out.print("\n+" + "-".repeat(20) + "+");
                      System.out.print("\n|3.Exit              |");
                      System.out.print("\n+" + "-".repeat(20) + "+");
                      int opt2=sc.nextInt();
                      
                      switch(opt2)
                      { //all customer show
                          case 1:
                             System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(7) +
                                     "+" + "-".repeat(15) + "+" + "-".repeat(8) + "+" + "-".repeat(8) + "+"+"-".repeat(9) +"+");
                             System.out.format("\n|%-5s|%-15s|%-12s|%-7s|%-15s|%-8s|%-8s|%-9s|", 
                                     "Id", "Name", "Phonenumber", "doorno", "streetname", "city", "pincode","Balance");
                             System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + 
                                     "+" + "-".repeat(7) + "+" + "-".repeat(15) + 
                                     "+" + "-".repeat(8) + "+" + "-".repeat(8) + "+"+"-".repeat(9)+"+");

                              for (int i = 0; i < sd.customer.length; i++) {
                                  if (sd.customer[i].status) {
                                  System.out.format("\n|%-5d|%-15s|%-12s|%-7d|%-15s|%-8s|%-8d|%9.2f|", 
                                      sd.customer[i].getCustomerId(), 
                                      sd.customer[i].getName(), sd.customer[i].getPhonenumber(), 
                                      sd.customer[i].getAddress().doorno,
                                      sd.customer[i].getAddress().streetname, 
                                      sd.customer[i].getAddress().city, 
                                      sd.customer[i].getAddress().pincode,
                                      sd.customer[i].getBalance());
                              }
                             }
                               System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+" 
                                       + "-".repeat(12) + "+" + "-".repeat(7) + "+" + "-".repeat(15) + "+" 
                                       + "-".repeat(8) + "+" + "-".repeat(8) + "+"+ "-".repeat(9) + "+");
                          break;
                          //single customer show
                         case 2:
                             System.out.print("\nEnter CustomerId :");
                             int reqcus=sc.nextInt();
                             System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(7) + "+" + "-".repeat(15) + "+" + "-".repeat(8) + "+" + "-".repeat(8) + "+"+ "-".repeat(9) + "+");
                             System.out.format("\n|%-5s|%-15s|%-12s|%-7s|%-15s|%-8s|%-8s|%-9s|", 
                                     "Id", "Name", "Phonenumber", "doorno", "streetname", "city", "pincode","Balance");
                             System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+" +
                                     "-".repeat(12) + "+" + "-".repeat(7) + "+" + "-".repeat(15) + "+" + 
                                     "-".repeat(8) + "+" + "-".repeat(8) + "+"+ "-".repeat(9) + "+");

                             for (int i=0;i<sd.customer.length;i++){
                                 if(sd.customer[i].getCustomerId()==reqcus)
                                 {
                                     System.out.print(sd.customer[i]);
                                     
                             }
                              
                             }
                             System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(15) + "+" + "-".repeat(12) + "+" + "-".repeat(7) + "+" + "-".repeat(15) + "+" + "-".repeat(8) + "+" + "-".repeat(8) + "+"+ "-".repeat(9) + "+");
                        break;
                        
                        case 3:
                        exits2=false;
                        break;
                        default:
                        System.out.print("\nInvalid input");
                       
                    }
                   
            }
                    break;

                case 3:
                    for (int i = 0; i < sd.customer.length; i++) {
                        if (!sd.customer[i].status) {

                            System.out.print(sd.customer[i].toString());
                        }
                        System.out.println("");
                    }

                    System.out.println("\nEnter the customerId :");
                    int delcustomerId = sc.nextInt();
                    for (int i = 0; i < sd.customer.length; i++) {
                        if (delcustomerId == sd.customer[i].getCustomerId()) {
                            sd.customer[i].status = false;
                        }
                    }

                    break;

                case 4:
                    System.out.println("Enter the customerId to modify :");
                    int modifyid = sc.nextInt();
                    int i = 0;
                    for (; i < sd.customer.length; i++) {
                        if (sd.customer[i].getCustomerId() == modifyid) {
                            break;
                        }
                    }
                    System.out.println("\n1.Name\n2.phonenumber\n3.add phonenumber\n4.doornumber\n5.streetname\n6.city\n7.pincode");

                    int modint = sc.nextInt();

                    switch (modint) {
                        case 1:
                            System.out.print("\nEnter new name :");
                            String mname = sc.next();
                            sd.customer[i].setName(mname);
                            break;
                        case 2:
                            System.out.print("\nEnter new phonenumber :");
                            String ph = sc.next();
                            String[] mphone = {ph};
                            sd.customer[i].setPhonenumber(mphone);
                            break;
                        case 3:
                            System.out.print("\nEnter additional phonenumber :");
                            String[] phone = sd.customer[i].addphonenum();
                            phone[phone.length - 1] = sc.next();
                            sd.customer[i].setPhonenumber(phone);
                            break;
                        case 4:
                            System.out.print("\nEnter new doornumber :");
                            sd.customer[i].getAddress().doorno = sc.nextInt();
                            break;
                        case 5:
                            System.out.println("\nEnter new street name :");
                            sd.customer[i].getAddress().streetname = sc.next();
                            break;
                        case 6:
                            System.out.println("\nEnter new city :");
                            sd.customer[i].getAddress().city = sc.next();
                            break;
                        case 7:
                            System.out.print("\nEnter new pincode :");
                            sd.customer[i].getAddress().pincode = sc.nextInt();
                            break;
                        default:
                            System.out.println("");

                    }

                    break;

                case 5:
                    ord.placeorder(sd);
                    break;

                case 6:
                       System.out.print("\nEnter the customerId :");
                       int paycusId=sc.nextInt();
                       for(int j=0;j<sd.customer.length;j++)
                       {
                         if(sd.customer[j].getCustomerId()==paycusId)
                         {System.out.print("\nYour pending balance :");
                             System.out.print(sd.customer[j].getBalance());
                             System.out.print("\nEnter the amount now you are paying :");
                             double pay=sc.nextDouble();
                            sd.customer[j].setBalance(sd.customer[j].getBalance()-pay);
                            System.out.print("\nYour balance is :");
                             System.out.print("\n"+sd.customer[j].getBalance());
                             sd.customer[j].setPaid(sd.customer[j].getPaid()+pay);
                         }
                       }
                       
                    break;
                case 7:
                    exit = false;
                    break;
                default:
                    System.out.println("Please enter the valid input");
            }

        }

    
    }

}
