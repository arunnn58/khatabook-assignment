
package khatabook;

import java.util.Scanner;

public class Statistics {
    
    Scanner sc=new Scanner(System.in);
    CusStatis cuss=new CusStatis();
    ProdSta prdst=new ProdSta();
    
    public void statis(ShopData sd)
    {
        
      boolean exit=true;
      
      while(exit)
      {
          
          System.out.print("\n+"+"-".repeat(21)+"+");
          System.out.print("\n|1.Customer Statistics|");
          System.out.print("\n+"+"-".repeat(21)+"+");
          System.out.print("\n|2.Product Statistics |");
          System.out.print("\n+"+"-".repeat(21)+"+");
          System.out.print("\n|3.Exit               |");
          System.out.print("\n+"+"-".repeat(21)+"+");
          int opt=sc.nextInt();
          switch(opt){
              case 1:
                       cuss.cussta(sd);
                  break;
              case 2:
                       prdst.prsta(sd);
                  break;
              case 3:
                  exit=false;
                  break;
              default:
                          System.out.print("\nPlease enter valid input...!");
          }  
      
      }
    
    }
    
}
