package khatabook;

import java.util.Scanner;

public class ProdSta {

    Scanner sc = new Scanner(System.in);

    ProductSold prdsol = new ProductSold();

    ProductSold[] prdssold = new ProductSold[0];

    ProductSold[] prdsolinc() {
        ProductSold[] tem = prdssold;
        prdssold = new ProductSold[prdssold.length + 1];
        for (int i = 0; i < tem.length; i++) {
            prdssold[i] = tem[i];
        }
        return prdssold;
    }

    public void prsta(ShopData sd) {
        boolean exit = true;

        while (exit) {
            System.out.print("\n+" + "-".repeat(29) + "+");
            System.out.print("\n|1.Product profit statistics  |");
            System.out.print("\n+" + "-".repeat(29) + "+");
            System.out.print("\n|2.Product stock details      |");
            System.out.print("\n+" + "-".repeat(29) + "+");
            System.out.print("\n|3.Exit                       |");
            System.out.print("\n+" + "-".repeat(29) + "+");

            int opt = sc.nextInt();

            switch (opt) {

                case 1:
                    boolean exit1 = true;
                    while (exit1) {
                        System.out.print("\n+" + "-".repeat(17) + "+");
                        System.out.print("\n|1.Sort by profit |");
                        System.out.print("\n+" + "-".repeat(17) + "+");
                        System.out.print("\n|2.Sort by sale   |");
                        System.out.print("\n+" + "-".repeat(17) + "+");
                        System.out.print("\n|3.Exit           |");
                        System.out.print("\n+" + "-".repeat(17) + "+");

                        int opt1 = sc.nextInt();
                        switch (opt1) {
                            case 1:
                                ProdSta set1 = prepare(sd);

                                ProductSold[] stbyprof = getstbyprof(set1.prdssold);

                                System.out.print("\n+" + "-".repeat(5) + "+"
                                        + "-".repeat(7) + "+" + "-".repeat(11) + "+"
                                        + "-".repeat(12) + "+" + "-".repeat(9) + "+" + "-".repeat(14) + "+" + "-".repeat(7) + "+");

                                System.out.format("\n|%-5s|%7s|%-11s|%-12s|%-9s|%-14s|%-7s|",
                                        "S.no", "ProdId", "ProductName", "Buying price", "Quantity", "Price of each", "Profit");

                                System.out.print("\n+" + "-".repeat(5) + "+"
                                        + "-".repeat(7) + "+" + "-".repeat(11) + "+"
                                        + "-".repeat(12) + "+" + "-".repeat(9) + "+" + "-".repeat(14) + "+" + "-".repeat(7) + "+");

                                for (int i = 0; i < stbyprof.length; i++) {
                                    System.out.format("\n|%5d%s", (i + 1), stbyprof[i]);

                                }

                                System.out.print("\n+" + "-".repeat(5) + "+"
                                        + "-".repeat(7) + "+" + "-".repeat(11) + "+"
                                        + "-".repeat(12) + "+" + "-".repeat(9) + "+" + "-".repeat(14) + "+" + "-".repeat(7) + "+");
                                break;
                            //sort by sold
                            case 2:
                                ProdSta set2 = prepare(sd);
                                ProductSold[] stbysal = getstbysal(set2.prdssold);

                                System.out.print("\n+" + "-".repeat(5) + "+"
                                        + "-".repeat(7) + "+" + "-".repeat(11) + "+" + "-".repeat(12) + "+" + "-".repeat(9) + "+" + "-".repeat(14) + "+" + "-".repeat(7) + "+");

                                System.out.format("\n|%-5s|%7s|%-11s|%-12s|%-9s|%-14s|%-7s|",
                                        "S.no", "ProdId", "ProductName", "Buying price", "Quantity", "Price of each", "Profit");

                                System.out.print("\n+" + "-".repeat(5) + "+"
                                        + "-".repeat(7) + "+" + "-".repeat(11) + "+" + "-".repeat(12) + "+" + "-".repeat(9) + "+" + "-".repeat(14) + "+" + "-".repeat(7) + "+");

                                for (int i = 0; i < stbysal.length; i++) {
                                    System.out.format("\n|%5d%s", (i + 1), stbysal[i]);
                                }

                                System.out.print("\n+" + "-".repeat(5) + "+"
                                        + "-".repeat(7) + "+" + "-".repeat(11) + "+" + "-".repeat(12) + "+" + "-".repeat(9) + "+" + "-".repeat(14) + "+" + "-".repeat(7) + "+");

                                break;
                            case 3:
                                exit1 = false;
                                break;
                            default:
                                System.out.print("\nPlease enter valid input");
                        }
                    }
                    break;

                case 2:
                    System.out.print("Entert productId :");
                    int prodId = sc.nextInt();
                    boolean absent = true;
                    for (int i = 0; i < sd.prods.length; i++) {
                        if (sd.prods[i].getProductId() == prodId) {
                            absent = false;
                            System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(20) + "+" + "-".repeat(15) + "+");
                            System.out.format("\n|%-5s|%-20s|%-15s|", "S.no", "Product Loaded Date", "Load Quantity");
                            System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(20) + "+" + "-".repeat(15) + "+");
                            for (int j = 0; j < sd.prods[i].stockload.length; j++) {
                                System.out.format("\n|%5d|%20s|%15d|", (j + 1), sd.prods[i].stockload[j].loaddate, sd.prods[i].stockload[j].stock);
                            }
                        }
                    }
                    if (absent) {
                        System.out.print("\nPlease enter correct productId...!");
                    } else {
                        System.out.print("\n+" + "-".repeat(5) + "+" + "-".repeat(20) + "+" + "-".repeat(15) + "+");
                    }

                    break;

                case 3:
                    exit = false;
                    break;
                default:
                    System.out.print("\nPlease enter valid input...!");
            }
        }
    }

    public ProdSta prepare(ShopData sd) {

        System.out.print("\nEnter month1 in number :");
        int month1 = sc.nextInt();
        System.out.print("\nEnter month2 in number :");
        int month2 = sc.nextInt();
        ProdSta obj = new ProdSta();
        obj.genSoldData(sd, month1, month2);

        return obj;

    }

    public void genSoldData(ShopData sd, int month1, int month2) {
        for (int i = 0; i < sd.ords.length; i++) {
            if (sd.ords[i].orddate.getMonthValue() >= month1 && sd.ords[i].orddate.getMonthValue() <= month2) {
                for (int j = 0; j < sd.ords[i].itemlist.length; j++) {
                    if (prodIdexist(sd.ords[i].itemlist[j].prodId)) {
                        int podindex = getprdsoldpos(sd.ords[i].itemlist[j].prodId);
                        prdssold[podindex].quantity += sd.ords[i].itemlist[j].quantity;
                        prdssold[podindex].price = sd.ords[i].itemlist[j].eachprice;
                    
                    } else {
                        prdssold = prdsolinc();
                        prdssold[prdssold.length - 1] = new ProductSold(sd.ords[i].itemlist[j].prodId, sd.ords[i].itemlist[j].quantity,
                                sd.ords[i].itemlist[j].eachprice,
                                getProductobj(sd, sd.ords[i].itemlist[j].prodId));
                    

                    }
                }

            }
        }
    }

    public ProductSold[] getstbyprof(ProductSold[] prds) {

        for (int i = 1; i < prds.length; i++) {
            if ((prds[i - 1].getProfit()) < prds[i].getProfit()) {
                ProductSold tem = prds[i - 1];
                prds[i - 1] = prds[i];
                prds[i] = tem;
                i = 0;
            }
        }
        return prds;
    }

    public ProductSold[] getstbysal(ProductSold[] prds) {

        for (int i = 1; i < prds.length; i++) {
            if (prds[i - 1].quantity < prds[i].quantity) {
                ProductSold tem = prds[i - 1];
                prds[i - 1] = prds[i];
                prds[i] = tem;
                i = 0;
            }
        }
        return prds;
    }

    public boolean prodIdexist(int id) {
        boolean exits = false;
        for (int i = 0; i < prdssold.length; i++) {
            if (prdssold[i].prodId == id) {
                exits = true;
                break;
            }

        }
        return exits;
    }

    public int getprdsoldpos(int id) {
        int i = 0;
        for (; i < prdssold.length; i++) {
            if (prdssold[i].prodId == id) {
                break;
            }

        }
        return i;
    }

    public Product getProductobj(ShopData sd, int id) {
        Product ob = null;
        for (int i = 0; i < sd.prods.length; i++) {
            if (id == sd.prods[i].getProductId()) {
                ob = sd.prods[i];
            }
        }
        return ob;
    }

}
