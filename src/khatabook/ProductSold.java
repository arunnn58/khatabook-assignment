package khatabook;

public class ProductSold {
    
    int prodId;
    int quantity;
    double price;
    double buyingprice;
    //double profit;
    String prodName;
    
    ProductSold(){}
    
    ProductSold(int prodId,int quantity,double price,Product obj)
    {
    this.prodId=prodId;
    this.quantity=quantity;
    this.price=price;
    this.buyingprice=obj.stockload[0].buyingprice;
    //this.profit=price*quantity-buyingprice*quantity;
    this.prodName=obj.getProductName();
    }
    
    public double getProfit(){
    
     return price*quantity-buyingprice*quantity;
    }
    
    public String toString(){
    
    //return "\n"+prodId+"  "+prodName+"  "+buyingprice+"  "+quantity+"  "+price+" "+getProfit();
     
//    System.out.print("\n+"+"-".repeat(5)+"+"
//                                        +"-".repeat(7)+"+"
//                                        +"-".repeat(11)+"+"+
//                                  "-".repeat(12)+"+"+"-".repeat(9)+"+"+"-".repeat(14)+"+"+"-".repeat(7)+"+");
      return String.format("|%-7d|%-11s|%12.2f|%-9d|%14.2f|%7.2f|",
              prodId,prodName,buyingprice,quantity,price,getProfit());
    
    }
    
}
