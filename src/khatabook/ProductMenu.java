package khatabook;

import java.time.LocalDate;
import java.util.Scanner;

public class ProductMenu {

    
    Orders ordss = new Orders();//to order method with parameter

    public Product[] addprod(Product[] prods) {

        Product[] tem = prods;
        prods = new Product[prods.length + 1];
        for (int i = 0; i < tem.length; i++) {
            prods[i] = tem[i];
        }

        return prods;
    }

    public void menu(ShopData sd) {
        

        //prodId, prodname, pri,brand
        Scanner sc = new Scanner(System.in);

        boolean exits = true;
        while (exits) {
            System.out.print("\nInventor");
            System.out.print("\n+---------------+");
            System.out.print("\n|1.Add product  |");
            System.out.print("\n+---------------+");
            System.out.print("\n|2.View products|");
            System.out.print("\n+---------------+");
            System.out.print("\n|3.Add stock    |");
            System.out.print("\n+---------------+");
            System.out.print("\n|4.Exit         |");
            System.out.print("\n+---------------+");

            int opt1 = sc.nextInt();

            switch (opt1) {
                case 1:

                    int prodId = 0;
                    if (sd.prods.length == 0) {
                        prodId = 501;
                    } else {
                        prodId = sd.prods[sd.prods.length - 1].getProductId() + 1;
                    }

                    System.out.print("\nEnter the Brandname :");
                    String brand = sc.next();

                    System.out.print("\nEnter product Name :");
                    String prodname = sc.next();
                    int stock = 0;

                    while (stock <= 0) {
                        System.out.print("\nEnter quantity :");
                        stock = sc.nextInt();
                        if (stock <= 0) {
                            System.out.print("\nPlease enter correct value of quantity");
                        }
                    }

                    System.out.print("\nEnter selling price :");
                    double pri = sc.nextDouble();
                    
                    System.out.print("\nEnter buying price :");
                    double bpri=sc.nextDouble();
                            

                    sd.prods = addprod(sd.prods);

                    sd.prods[sd.prods.length - 1] = new Product(prodId, prodname, pri, brand, stock);
                    sd.prods[sd.prods.length-1].stockload=sd.prods[sd.prods.length-1].incsto();
                    sd.prods[sd.prods.length-1].stockload[sd.prods[sd.prods.length-1].stockload.length-1]=new Stock(stock,LocalDate.now(),bpri);

                    System.out.print("\n\nProduct added successfully...!");
                    break;

                case 2:
                    //title of product table
                    System.out.print("\n+"+"-".repeat(8)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"
                    +"-".repeat(8)+"+"+"-".repeat(14)+"+");
                    System.out.format("\n|%-8s|%-20s|%-20s|%8s|%-14s|", 
                            "ProdId","Brand","ProdName","Price","Available");
                    System.out.print("\n+"+"-".repeat(8)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"
                    +"-".repeat(8)+"+"+"-".repeat(14)+"+");
                    
                    //product table content
                    for (int i = 0; i < sd.prods.length; i++) {
                        System.out.print(sd.prods[i]);
                    }
                    
                    System.out.print("\n+"+"-".repeat(8)+"+"+"-".repeat(20)+"+"+"-".repeat(20)+"+"
                    +"-".repeat(8)+"+"+"-".repeat(14)+"+");
                    
                    
                    
                    break;
                case 3:
                    System.out.print("\nEnter the productId :");
                    int stkid = sc.nextInt();
                    int i = 0;
                    for (; i < sd.prods.length; i++) {
                        if (stkid == sd.prods[i].getProductId()) {
                            System.out.print("Enter the new stock quantity :");
                            int k = sc.nextInt();
                            System.out.print("Enter buying price :");
                            double bupri=sc.nextDouble();
                            
                            sd.prods[i].available += k;
                            sd.prods[i].stockload = sd.prods[i].incsto();
                            sd.prods[i].stockload[sd.prods[i].stockload.length-1]=new Stock(k,LocalDate.now(),bupri);
                            break;
                        }
                    }

                    break;
                case 4:
                   
                    exits = false;
                    break;
                default:
                    System.out.println("Please enter valid input");

            }

        }
        //return prods;

    }

   

}
