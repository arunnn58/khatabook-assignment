
package khatabook;

import java.time.LocalDate;

public class Stock {
    
    int stock;
    LocalDate loaddate;
    double buyingprice;
    
    Stock(){}
    
    Stock(int stock,LocalDate loaddate,double buyingprice){
    this.stock=stock;
    this.buyingprice=buyingprice;
    this.loaddate=loaddate;
    }
    
}
